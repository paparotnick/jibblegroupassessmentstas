# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Technical Assesments for Jibble Group
* 1.0

### How do I get set up? ###

#### FIRST: Repository

[Guide to clone repo](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)

git clone git@bitbucket.org:paparotnick/jibblegroupassessmentstas.git

#### NEXT: CocoaPods

[Guide to install CocoaPods](https://guides.cocoapods.org/using/getting-started.html)

[Guide to use CocoaPods](https://guides.cocoapods.org/using/using-cocoapods.html)


## About Dependencies

[RxSwift & RxCocoa](https://github.com/ReactiveX/RxSwift) - Reactive Swift

[SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - Mapping Data from API


### Who do I talk to? ###

* Stanislav Voronov, staas.voronov@gmail.com