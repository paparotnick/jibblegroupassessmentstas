//
//  ViewController.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UsersListViewController: UITableViewController {

    // MARK: - IBOutlets
    
    // MARK: - ivars
    
    var viewModel: UsersListViewModel = UsersListViewModel(dataService: JBDataService(networkingService: HTTPNetworking()))
    
    private let disposeBag = DisposeBag()
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initData()
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
    }
    
    // MARK: - Inits
    
    private func updateUI() {
        
    }
    
    private func initUI() {
    }
    
    private func initData() {
        configureTableView()
        setupBindingDataToTableView()
        setUpBindTableViewSelected()
        bindErrors()
        viewModel.requestData()
    }
    
    private func configureTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.delegate = nil
        tableView.dataSource = nil
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - SetUp Bindings
    
    private func setUpBindTableViewSelected() {
        
        tableView
            .rx
            .modelSelected(JBUser.self)
            .subscribe(onNext :{ [weak self] user in
                
                guard let strongSelf = self else { return }
                
                guard let userDetailsVC = strongSelf.storyboard?.instantiateViewController(withIdentifier: "UserDetailsViewController") as? UserDetailsViewController else {
                    fatalError("UserDetailsViewController not found")
                }
                
                if let selectedRowIndexPath = strongSelf.tableView.indexPathForSelectedRow {
                  strongSelf.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
                
                userDetailsVC.viewModel = UserDetailsViewModel(user: user)
                
                strongSelf.navigationController?.pushViewController(userDetailsVC, animated: true)
                
            }).disposed(by: disposeBag)

    }
    
    private func setupBindingDataToTableView(){
        
        viewModel
            .users
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(cellIdentifier: "UserTableViewCell", cellType: UserTableViewCell.self)) {  (row, user, cell) in
                cell.bind(UserCellViewModel(user: user))
            }.disposed(by: disposeBag)
            
    }
    
    private func bindErrors() {
        viewModel
        .error
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { (error) in
            switch error {
            case .internetError(let message):
                self.showErrorAlert(message)
            case .serverMessage(let message):
                self.showErrorAlert(message)
            case .mappingError(let message):
                self.showErrorAlert(message)
            }
        })
        .disposed(by: disposeBag)
    }
    
    // MARK: - Actions
    
    func showErrorAlert(_ message:String) {
        let alertController = UIAlertController(title: "Error", message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))

        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    // MARK: - Transition


}

