//
//  UserTableViewCell.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userName : UILabel!
    @IBOutlet weak var userRole: UILabel!
    
    private let disposeBag = DisposeBag()
    
    func bind(_ viewModel: UserCellViewModel) {
        viewModel.userName.bind(to: userName.rx.text).disposed(by: disposeBag)
        viewModel.userRole.bind(to: userRole.rx.text).disposed(by: disposeBag)
    }

}
