//
//  UserDetailsViewController.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation
import RxSwift

class UserDetailsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    var viewModel: UserDetailsViewModel!
    
    // MARK: - ivars
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initData()
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
    }
    
    // MARK: - Inits
    
    private func updateUI() {
        
    }
    
    private func initUI() {
        bindLabels()
    }
    
    private func initData() {
        
    }
    
    // MARK: - Bindings
    
    private func bindLabels() {
        viewModel.userName.bind(to: usernameLabel.rx.text).disposed(by: disposeBag)
        viewModel.userDeparment.bind(to: emailLabel.rx.text).disposed(by: disposeBag)
        viewModel.userRole.bind(to: roleLabel.rx.text).disposed(by: disposeBag)
        viewModel.userEmail.bind(to: departmentLabel.rx.text).disposed(by: disposeBag)
    }
    
    // MARK: - Actions
    // MARK: - IBActions
    // MARK: - Transition

    
}
