//
//  UsersListViewModel.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class UsersListViewModel {
    
    let dataService: JBDataService

    init(dataService: JBDataService) {
        self.dataService = dataService
    }
    
    enum UsersListError {
        case mappingError(String)
        case internetError(String)
        case serverMessage(String)
    }
    
    let users : PublishSubject<[JBUser]> = PublishSubject()
    let loading: PublishSubject<Bool> = PublishSubject()
    let error : PublishSubject<UsersListError> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    func requestData(){
        
        self.loading.onNext(true)
        self.dataService.requestUsersList(completion: { (result) in
            self.loading.onNext(false)
            switch result {
            case .success(let returnJson) :
                do {
                    let users = try JSONDecoder().decode([JBUser].self, from: returnJson.rawData()).filter { $0.deleted == false }
                    self.users.onNext(users)
                } catch {
                    self.error.onNext(.mappingError("Error while mapping data"))
                }
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error.onNext(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    self.error.onNext(.serverMessage(errorJson["error"]["message"].stringValue))
                default:
                    self.error.onNext(.serverMessage("Unknown Error"))
                }
            }
        })
        
    }
}

