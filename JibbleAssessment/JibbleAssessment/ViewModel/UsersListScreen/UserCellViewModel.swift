//
//  UserCellViewModel.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

final class UserCellViewModel {
    
    let userName: Observable<String>
    let userRole: Observable<String>

    init(user: JBUser) {
        self.userName = Observable<String>.just(user.name)
        self.userRole = Observable<String>.just(user.role)
    }
}
