//
//  UserDetailsViewModel.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

final class UserDetailsViewModel {
    
    lazy var userName: Observable<String> = Observable<String>.just("Name: \(self.user.name)")
    lazy var userEmail: Observable<String> = Observable<String>.just("Email: \(self.user.email)")
    lazy var userDeparment: Observable<String> = Observable<String>.just("Department: \(self.user.department)")
    lazy var userRole: Observable<String> = Observable<String>.just("Role: \(self.user.role)")

    let user: JBUser
    
    init(user: JBUser) {
        self.user = user
    }
}
