//
//  Users.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation

struct JBUser: Codable {
    let id, name, email, role, department: String
    let deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, role, department, deleted
    }
}

extension JBUser {
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(JBUser.self, from: data)
            self = me
        }
        catch {
            return nil
        }
    }
}
