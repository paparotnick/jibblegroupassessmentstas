//
//  NetworkingService.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

protocol NetworkingService {
  typealias CompletionHandler = (Data?, URLResponse?, Swift.Error?) -> Void
  
  func request(from: String, completion: @escaping CompletionHandler)
}

struct HTTPNetworking: NetworkingService {
    
  func request(from: String, completion: @escaping CompletionHandler) {
    guard let url = URL(string: from) else { return }
    let request = createRequest(from: url)
    let task = createDataTask(from: request, completion: completion)
    task.resume()
  }
  
  private func createRequest(from url: URL) -> URLRequest {
    var request = URLRequest(url: url)
    request.cachePolicy = .reloadIgnoringCacheData
    return request
  }
  
  private func createDataTask(from request: URLRequest,
                              completion: @escaping CompletionHandler) -> URLSessionDataTask {
    return URLSession.shared.dataTask(with: request) { data, httpResponse, error in
      completion(data, httpResponse, error)
    }
  }
}


