//
//  JBAPIService.swift
//  JibbleAssessment
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import Foundation

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

struct JBDataService {
    
    let networkingService: NetworkingService

    init(networkingService: NetworkingService) {
        self.networkingService = networkingService
    }

    let usersEndPoint = "https://79a44aa1-c4f1-4a51-b7d7-151620d99df0.mock.pstmn.io/users"
        
    enum ApiResult {
        case success(JSON)
        case failure(RequestError)
    }

    enum RequestError: Error {
        case unknownError
        case connectionError
        case authorizationError(JSON)
        case invalidRequest
        case notFound
        case invalidResponse
        case serverError
        case serverUnavailable
    }
        
    func requestUsersList(completion: @escaping (ApiResult) -> Void) {

        networkingService.request(from: self.usersEndPoint) { (data, response, error) in
            if error != nil {
                completion(ApiResult.failure(.connectionError))
            } else if let data = data ,let responseCode = response as? HTTPURLResponse {
                do {
                    let responseJson = try JSON(data: data)
                    switch responseCode.statusCode {
                    case 200:
                        completion(ApiResult.success(responseJson))
                    case 400...499:
                        completion(ApiResult.failure(.authorizationError(responseJson)))
                    case 500...599:
                        completion(ApiResult.failure(.serverError))
                    default:
                        completion(ApiResult.failure(.unknownError))
                        break
                    }
                }
                catch _ {
                    completion(ApiResult.failure(.unknownError))
                }
            }
        }
    }

}
