//
//  NetworkingServiceMock.swift
//  JibbleAssessmentTests
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import XCTest

@testable import JibbleAssessment

struct NetworkingServiceMock: NetworkingService {
  let filename: String
  
  func request(from: String, completion: @escaping CompletionHandler) {
    let data = readJSON(name: filename)
    let urlResponse = HTTPURLResponse(url: URL(fileURLWithPath: filename), statusCode: 200, httpVersion: nil, headerFields: nil)
    completion(data, urlResponse, nil)
  }
  
  private func readJSON(name: String) -> Data? {
    let bundle = Bundle(for: JBDataServiceTest.self)
    guard let url = bundle.url(forResource: name, withExtension: "json") else { return nil }
    
    do {
      return try Data(contentsOf: url, options: .mappedIfSafe)
    }
    catch {
      XCTFail("Error occurred parsing test data")
      return nil
    }
  }
}

enum DataSet: String {
    case one
    case two

    static let all: [DataSet] = [.one, .two]
}

extension DataSet {
    var name: String { return rawValue }
    var filename: String { return "dataset-\(rawValue)" }
}
