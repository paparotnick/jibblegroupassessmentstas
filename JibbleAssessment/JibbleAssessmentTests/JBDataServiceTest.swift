//
//  JBDataServiceTest.swift
//  JibbleAssessmentTests
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import XCTest

@testable import JibbleAssessment


class JBDataServiceTest: XCTestCase {


    override func setUp() {
        super.setUp()
        
    }

    override func tearDown() {
        super.tearDown()
    }
    

    func testLoadDataFromDataSetOne() {
    
        //given
        
        let dataService = JBDataService(networkingService: NetworkingServiceMock(filename: DataSet.one.filename))
        let expectation = XCTestExpectation(description: "Fetch User List from Dataset one")

        //when

        dataService.requestUsersList { (result) in
            
            switch result {
            case .success(let returnJson):
                XCTAssertEqual(10, returnJson.array?.count)
                expectation.fulfill()
            case .failure( _) :
                XCTAssert(true)
                expectation.fulfill()
            }
        }
        
                
        wait(for: [expectation], timeout: 1.0)

    }
    
    func testLoadDataFromDataSetTwo() {
    
        //given
        
        let dataService = JBDataService(networkingService: NetworkingServiceMock(filename: DataSet.two.filename))
        let expectation = XCTestExpectation(description: "Fetch User List from Dataset one")

        //when

        dataService.requestUsersList { (result) in
            
            switch result {
            case .success(let returnJson):
                XCTAssertEqual(5, returnJson.array?.count)
                expectation.fulfill()
            case .failure( _) :
                XCTAssert(true)
                expectation.fulfill()
            }
        }
        
                
        wait(for: [expectation], timeout: 1.0)

    }


}
