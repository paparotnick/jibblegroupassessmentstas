//
//  JBUserListViewModelTest.swift
//  JibbleAssessmentTests
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import XCTest
import RxCocoa
import RxSwift

@testable import JibbleAssessment

class JBUserListViewModelTest: XCTestCase {
    
    private let disposeBag = DisposeBag()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDataSetOne() {
        
        
        //given
        let dataServiceMock = JBDataService(networkingService: NetworkingServiceMock(filename: DataSet.one.filename))
        let viewModel = UsersListViewModel(dataService: dataServiceMock)
        
        let users = PublishSubject<[JBUser]>()

        let collector = RxCollector<[JBUser]>().collect(from: users)
        
        viewModel
        .users
        .observeOn(MainScheduler.instance)
        .bind(to: users).disposed(by: disposeBag)
        
        //when
        
        viewModel.requestData()
        
        
        //then
        XCTAssertEqual(8, collector.toArray.first!.count)
        XCTAssertEqual("Julia Martinez", collector.toArray.first!.first!.name)
    }
    
    func testDataSetTwo() {
        
        //given
        
        let dataServiceMock = JBDataService(networkingService: NetworkingServiceMock(filename: DataSet.two.filename))
        let viewModel = UsersListViewModel(dataService: dataServiceMock)
        
        let users = PublishSubject<[JBUser]>()

        let collector = RxCollector<[JBUser]>().collect(from: users)
        
        viewModel
        .users
        .observeOn(MainScheduler.instance)
        .bind(to: users).disposed(by: disposeBag)
        
        //when
        
        viewModel.requestData()
        
        
        //then
        
        XCTAssertEqual(4, collector.toArray.first!.count)
        XCTAssertEqual("Julia Martinez", collector.toArray.first!.first!.name)
        
    }
    
}
