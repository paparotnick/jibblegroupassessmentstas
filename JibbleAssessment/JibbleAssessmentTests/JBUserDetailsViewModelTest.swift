//
//  UserDetailsViewModel.swift
//  JibbleAssessmentTests
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import XCTest
import RxSwift

@testable import JibbleAssessment

class JBUserDetailsViewModelTest: XCTestCase {

    private let disposeBag = DisposeBag()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewModelStrings() {
        
        //given
        
        let mockUser = JBUser(id: "-1", name: "test", email: "test@test.test", role: "QA", department: "IT", deleted: true)
        
        //when
        
        let viewModel = UserDetailsViewModel(user: mockUser)
        
        let collectorRole = RxCollector<String>().collect(from: viewModel.userRole)
        let collectorEmail = RxCollector<String>().collect(from: viewModel.userEmail)
        let collectorDepartment = RxCollector<String>().collect(from: viewModel.userDeparment)
        let collectorName = RxCollector<String>().collect(from: viewModel.userName)
        
        
        //then
        
        XCTAssertEqual("Role: QA", collectorRole.toArray.first!)
        XCTAssertEqual("Email: test@test.test", collectorEmail.toArray.first!)
        XCTAssertEqual("Department: IT", collectorDepartment.toArray.first!)
        XCTAssertEqual("Name: test", collectorName.toArray.first!)
    }


}
