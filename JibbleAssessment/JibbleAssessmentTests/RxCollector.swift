//
//  RxCollector.swift
//  JibbleAssessmentTests
//
//  Created by paparotnick on 2/26/20.
//  Copyright © 2020 staas. All rights reserved.
//

import RxCocoa
import RxSwift

class RxCollector<T> {
    var deadBodies: DisposeBag = DisposeBag()
    var toArray: [T] = [T]()
    func collect(from observable: Observable<T>) -> RxCollector {
        observable.asObservable()
        .subscribe(onNext: { (newItem) in
             self.toArray.append(newItem)
        }).disposed(by: deadBodies)
       return self
    }
}

